import { motion } from 'framer-motion'
import { ReactNode } from 'react'
import { twMerge } from 'tailwind-merge'

interface IPageLayout {
  children: ReactNode
  className?: string
}

const PageWrapper = ({ children, className }: IPageLayout) => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1, transition: { duration: 0.7 } }}
      exit={{ opacity: 0 }}
      className={twMerge(
        'flex relative min-h-max w-full flex-col grow',
        className,
      )}
    >
      {children}
    </motion.div>
  )
}

export { PageWrapper }
