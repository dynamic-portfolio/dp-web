import { createBrowserRouter } from 'react-router-dom'

import { lazyImport } from '@/utils/lazyImport'
import { withSuspense } from '@/hocs/withSuspense'

const { CurriculumRoutes } = lazyImport(
  () => import('@/features/curriculum'),
  'CurriculumRoutes'
)

const Root = withSuspense(() => {
  return (
    <>
      <CurriculumRoutes />
    </>
  )
})



export const appRouter = createBrowserRouter([
  {
    path: '*',
    Component: Root,
  },
])
