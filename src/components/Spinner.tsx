import { Spinner as NUISpinner, SpinnerProps } from '@nextui-org/react'

const Spinner = (_props: SpinnerProps) => {
  return (
    <NUISpinner
      color='primary'
    />
  )
}

export { Spinner }
