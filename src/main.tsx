import ReactDOM from 'react-dom/client'
import { RouterProvider } from 'react-router-dom'

import { AppProvider } from '@/providers'
import '@/index.css'

import { appRouter } from './routes'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <AppProvider>
    <RouterProvider router={appRouter} />
  </AppProvider>,
)
