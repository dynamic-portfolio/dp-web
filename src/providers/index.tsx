import * as React from 'react'
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { NextUIProvider } from '@nextui-org/react';
import { AnimatePresence } from 'framer-motion';
import { ToastContainer } from 'react-toastify';

const queryClient = new QueryClient()

export const AppProvider = ({ children }: { children: React.ReactNode }) => {
  return (
    <QueryClientProvider client={queryClient}>
      <AnimatePresence>
        <NextUIProvider>
          {children}
          <ToastContainer />
        </NextUIProvider>
      </AnimatePresence>
    </QueryClientProvider>
  )
}