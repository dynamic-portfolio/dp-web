import axios, { AxiosError } from 'axios'
import { ENV } from '@/constants'

export type IApiResponse<T> = {
  message: string
  detail: T
}

export type RawApiError = AxiosError<{
  code: number
  message: string
  detail: Record<string, unknown>
}>

export const axiosInstance = axios.create({
  baseURL: ENV.API_URL,
})

